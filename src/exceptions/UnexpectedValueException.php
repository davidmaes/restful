<?php

namespace davidmaes\restful\exceptions;

class UnexpectedValueException extends \UnexpectedValueException implements IException
{
}
