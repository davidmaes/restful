<?php

namespace davidmaes\restful\exceptions;

class InvalidArgumentException extends \InvalidArgumentException implements IException
{
}
