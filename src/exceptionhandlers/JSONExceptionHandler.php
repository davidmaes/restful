<?php

namespace davidmaes\restful\exceptionhandlers;

use Exception;
use davidmaes\restful\response\json\JSON;
use davidmaes\restful\response\Response;
use stdClass;

class JSONExceptionHandler implements IExceptionHandler
{
    /**
     * Handles exceptions uncaught by controllers.
     *
     * @param Exception $e The exception that was thrown.
     *
     * @return Response The response that should be returned when the exception occurs.
     */
    function handleException(Exception $e)
    {
        $response = new JSON();
        $response->setStatus($e->getCode());

        $data = new stdClass();
        $data->code = $e->getCode();
        $data->message = $e->getMessage();

        $response->setData($data);

        return $response;
    }

}