<?php

    namespace davidmaes\restful\exceptionhandlers;

    use Exception;
    use davidmaes\restful\response\Response;

    interface IExceptionHandler
    {
        /**
         * Handles exceptions uncaught by controllers.
         *
         * @param Exception $e The exception that was thrown.
         *
         * @return Response The response that should be returned when the exception occurs.
         */
        function handleException(Exception $e);
    }