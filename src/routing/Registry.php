<?php

    namespace davidmaes\restful\routing;

    class Registry
    {
        /**
         * @var Route[] An array of routes.
         */
        private $routes;

        /**
         * Registry constructor.
         */
        public function __construct()
        {
            $this->routes = [];
        }

        /**
         * Registers a new route.
         *
         * @param Route $route The route to save in this registry.
         */
        public function registerRoute(Route $route)
        {
            $this->routes[] = $route;
        }

        /**
         * Returns a matching route for a given HTTP method and URI.
         *
         * @param string $method The method that should match one of the registered routes.
         * @param string $uri The URI that should match one of the registered routes.
         * @return Route The route that matches both the HTTP method and URI. Returns null if no matches were found.
         */
        public function getMatchingRoute(string $method, string $uri)
        {
            foreach ($this->routes as $route) {
                if ($method !== $route->getMethod()) {
                    continue;
                }

                if ($this->matchSignature($route->getSignature(), $uri)) {
                    return $route;
                }
            }

            return null;
        }

        /**
         * Checks if a signature and URI match. Note that parameters are extracted at the same time as this makes sure we
         * only have to parse the URI once.
         *
         * @param string $signature The signature to match the given uri
         * @param string $uri The URI that will be checked against the signature.
         * @return bool True if the URI matches the signature, false if it doesn't.
         */
        private function matchSignature(string $signature, string $uri)
        {
            return $this->matchSignatureParts(
                explode('/', trim($signature, '/')),
                explode('/', trim($uri, '/'))
            );
        }

        /**
         * Help method of matchSignature. This does the actual work but only after the URI and signature are broken up
         * into an array for every node.
         *
         * @param string[] $signature The signature to match the given uri.
         * @param string[] $uri The URI that will be checked against the signature.
         * @return bool True if the URI matches the signature, false if it doesn't.
         */
        private function matchSignatureParts($signature, $uri)
        {
            if (count($signature) !== count($uri)) {
                return false;
            }

            for ($i = 0; $i < count($signature); $i++) {
                if (preg_match('/^\{[A-Za-z]*\}$/', $signature[$i])) {
                    continue;
                } elseif ($signature[$i] !== $uri[$i]) {
                    return false;
                }
            }

            return true;
        }

    }