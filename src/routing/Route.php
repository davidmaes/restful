<?php

    namespace davidmaes\restful\routing;

    class Route
    {
        /**
         * @var string The HTTP method (POST, GET, PUT, DELETE) for the route.
         */
        private $method;

        /**
         * @var string The URI signature that matches this route.
         */
        private $signature;

        /**
         * @var string The class that should be instantiated when the method and URI are matched.
         */
        private $class;

        /**
         * @var string The name of the method that will be called when the method and URI are matched.
         */
        private $callback;

        /**
         * Route constructor.
         *
         * @param string $method The HTTP method the route will match on.
         * @param string $signature The signature the route will match on.
         * @param string $class The controller that should be executed when this route matches.
         * @param string $callback The callback method that will be called on the controller.
         */
        public function __construct(
            string $method,
            string $signature,
            string $class,
            string $callback
        ) {
            $this->method = $method;
            $this->signature = $signature;
            $this->class = $class;
            $this->callback = $callback;
        }

        /**
         * Returns the HTTP method of the route.
         *
         * @return string The HTTP method of the route.
         */
        public function getMethod()
        {
            return $this->method;
        }

        /**
         * Returns the URI signature of the route.
         *
         * @return string The URI signature of the route.
         */
        public function getSignature()
        {
            return $this->signature;
        }

        /**
         * Returns the complete class name (including namespace) for the controller class.
         *
         * @return string The class name of the controller.
         */
        public function getClass()
        {
            return $this->class;
        }

        /**
         * Returns the method that will be called on the controller class.
         *
         * @return string
         */
        public function getCallback()
        {
            return $this->callback;
        }
    }
