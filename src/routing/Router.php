<?php

namespace davidmaes\restful\routing;

use Error;
use Exception;
use davidmaes\restful\exceptions\IException;
use davidmaes\restful\response\HTTPStatusCode;
use davidmaes\restful\response\Response;
use stdClass;

class Router
{
    /**
     * @var Registry The service that holds all the routes for the router.
     */
    private $registry;

    /**
     * Router constructor.
     *
     * @param Registry $registry The service that holds all the routes for the router.
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Get the controller for a given route. If none can be found, the NotFoundController will be executed.
     *
     * @param string $method The HTTP method of the request.
     * @param string $uri The URI of the request.
     *
     * @return Response A response to be rendered as output.
     *
     * @throws Exception If no route was found, an exception is thrown.
     */
    public function route(string $method, string $uri)
    {
        $route = $this->registry->getMatchingRoute($method, $uri);

        if (!$route) {
            throw new Exception('Not Found', HTTPStatusCode::HTTP_NOT_FOUND);
        }

        $class = $route->getClass();
        $callback = $route->getCallback();
        $controller = new $class();
        $parameters = $this->getParameters($route->getSignature(), $uri);

        try {
            return call_user_func_array([$controller, $callback], $parameters);
        } catch (IException $e) {
            throw new Exception($e->getMessage(), HTTPStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            throw new Exception('Internal Server Error', HTTPStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Error $e) {
            throw new Exception('Internal Server Error', HTTPStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Retrieves both GET and POST parameters. GET parameters are parsed from the URI whenever they match the signature.
     * POST variables are parsed from the PHP input stream.
     *
     * @param string $signature The signature of the route that matched.
     * @param string $uri The URI of the request.
     * @return mixed[] An array of both GET and POST variables. Note that the POST variable (if available) is always the last
     * entry in the array.
     */
    private function getParameters($signature, $uri)
    {
        $parameters = $this->parseGet(
            explode('/', trim($signature, '/')),
            explode('/', trim($uri, '/'))
        );

        $parameters[] = $this->parsePost();

        return $parameters;
    }

    /**
     * Parses the GET variables from the URI.
     *
     * @param string[] $signature The signature with all the variable names.
     * @param string[] $uri The URI with all the variable values.
     * @return string[] An array with all the variable values in the URI.
     */
    private function parseGet($signature, $uri)
    {
        $variables = [];

        for ($i = 0; $i < count($signature); $i++) {
            if (preg_match('/^\{[A-Za-z]*\}$/', $signature[$i])) {
                $variables[] = $uri[$i];
            }
        }

        return $variables;
    }

    /**
     * Parses the POST variables. Note that we assume the POST data will always be JSON as a convention.
     *
     * @return stdClass The POST/PUT variables passed with the request.
     */
    private function parsePost()
    {
        return json_decode(file_get_contents('php://input'));
    }
}
