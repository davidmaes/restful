<?php

    namespace davidmaes\restful;

    use Exception;
    use davidmaes\config\Config;
    use davidmaes\restful\exceptionhandlers\IExceptionHandler;
    use davidmaes\restful\exceptionhandlers\JSONExceptionHandler;
    use davidmaes\restful\routing\Registry;
    use davidmaes\restful\routing\Route;
    use davidmaes\restful\routing\Router;

    class Restful
    {
        /**
         * @var Registry The routing registry that holds the defined routes in this microservice.
         */
        private $registry;

        /**
         * @var Router The router that will take any request and direct it to the proper controller.
         */
        private $router;

        /**
         * @var string The path to the routes configuration file.
         */
        private $routesConfigurationPath;

        /**
         * @var IExceptionHandler The exception handler that
         */
        private $routerExceptionHandler;

        /**
         * Restful constructor.
         */
        public function __construct()
        {
            $this->registry = new Registry();
            $this->router = new Router($this->registry);
            $this->routesConfigurationPath = '../config/routes.json';
            $this->routerExceptionHandler = new JSONExceptionHandler();
        }

        /**
         * Sets the routes configuration path.
         *
         * @param string $path The path to the routes configuration file.
         */
        public function setRoutesConfigurationPath(string $path)
        {
            $this->routesConfigurationPath = $path;
        }

        /**
         * Sets the exception handler for uncaught exceptions.
         *
         * @param IExceptionHandler $handler
         */
        public function setRouterExceptionHandler(IExceptionHandler $handler)
        {
            $this->routerExceptionHandler = $handler;
        }

        /**
         * @param string $method
         * @param string $uri
         */
        public function handleRequest(string $method, string $uri)
        {
            $this->registerRoutes();

            try {
                $response = $this->router->route($method, $uri);
            } catch (Exception $e) {
                $response = $this->routerExceptionHandler->handleException($e);
            }

            $response->render();
        }

        /**
         * Bootstraps the routes retrieved from getRoutes.
         */
        private function registerRoutes()
        {
            $config = new Config();
            $routes = $config->get($this->routesConfigurationPath);

            foreach ($routes as $route) {
                $this->registry->registerRoute(
                    new Route(
                        $route->method,
                        $route->signature,
                        $route->class,
                        $route->callback
                    )
                );
            }
        }
    }
