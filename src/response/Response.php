<?php

    namespace davidmaes\restful\response;

    abstract class Response
    {
        /**
         * @var int The HTTP response status.
         */
        protected $status;

        public function __construct()
        {
            $this->status = HTTPStatusCode::HTTP_OK;
        }

        /**
         * Sets the HTTP response status.
         *
         * @param int $status The HTTP response status.
         */
        public function setStatus(int $status)
        {
            $this->status = $status;
        }

        abstract public function render();
    }