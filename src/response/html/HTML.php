<?php

namespace davidmaes\restful\response\html;

use davidmaes\restful\response\Response;
use Twig_Environment;
use Twig_Loader_Filesystem;

class HTML extends Response
{
    /**
     * @var string The template to render.
     */
    private $template;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array
     */
    private $variables;

    /**
     * HTML constructor.
     *
     * @param string $template The path to the template to be loaded.
     * @param array $variables
     */
    public function __construct(String $template, array $variables)
    {
        parent::__construct();

        $this->twig = new Twig_Environment(new Twig_Loader_Filesystem('../private_php/templates'));

        $this->template = $template;
        $this->variables = $variables;
    }

    public function render()
    {
        header("Content-Type:text/html");

        $template = $this->twig->load($this->template);
        $template->display($this->variables);
    }

}