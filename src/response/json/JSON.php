<?php

    namespace davidmaes\restful\response\json;

    use davidmaes\restful\response\Response;
    use stdClass;

    class JSON extends Response
    {
        /**
         * @var stdClass|array $data The data of the response.
         */
        private $data;

        /**
         * JSON constructor.
         */
        public function __construct()
        {
            parent::__construct();

            $this->data = new stdClass();
        }

        /**
         * @param stdClass|array $data
         */
        public function setData($data)
        {
            $this->data = $data;
        }

        /**
         * {@inheritdoc}
         */
        public function render()
        {
            header('Content-type: application/json');
            http_response_code($this->status);
            echo json_encode($this->data, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        }

    }