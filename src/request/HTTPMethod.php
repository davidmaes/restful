<?php

    namespace davidmaes\restful\request;

    class HTTPMethod
    {
        const GET = "GET";
        const POST = "POST";
        const PUT = "PUT";
        const DELETE = "DELETE";
    }