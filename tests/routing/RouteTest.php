<?php

    use PHPUnit\Framework\TestCase;
    use davidmaes\restful\request\HTTPMethod;
    use davidmaes\restful\routing\Route;

    class RouteTest extends TestCase
    {
        public function test___constructor_parameters()
        {
            $method = HTTPMethod::GET;
            $signature = '/test/uri/signature';
            $class = 'davidmaes\xxx\controllers\Class';
            $callback = 'execute';

            $route = new Route(
                $method,
                $signature,
                $class,
                $callback
            );

            $this->assertEquals($method, $route->getMethod());
            $this->assertEquals($signature, $route->getSignature());
            $this->assertEquals($class, $route->getClass());
            $this->assertEquals($callback, $route->getCallback());
        }
    }
