<?php

    use PHPUnit\Framework\TestCase;
    use davidmaes\restful\request\HTTPMethod;
    use davidmaes\restful\routing\Registry;
    use davidmaes\restful\routing\Route;

    class RegistryTest extends TestCase
    {
        /**
         * @var Registry The registry to save routes.
         */
        private $registry;

        /**
         * RegistryTest setUp.
         */
        public function setUp()
        {
            parent::setUp();

            $this->registry = new Registry();
        }

        /**
         * Tests if the registry finds a route that was previously specified.
         */
        public function test_getMatchingRoute_validRouteNoParameters()
        {
            $route = new Route(HTTPMethod::GET, '/test/uri/signature', '', '');

            $this->registry->registerRoute($route);

            $this->assertEquals(
                $route,
                $this->registry->getMatchingRoute(
                    HTTPMethod::GET,
                    '/test/uri/signature'
                )
            );
        }

        /**
         * Tests if the registry finds a route that was previously specified.
         */
        public function test_getMatchingRoute_validRouteRandomParameters()
        {
            $route = new Route(HTTPMethod::GET, '/test/{testId}/sub/{subId}', '', '');

            $this->registry->registerRoute($route);

            $this->assertEquals(
                $route,
                $this->registry->getMatchingRoute(
                    HTTPMethod::GET,
                    '/test/' . $this->generateRandomString() . '/sub/' . $this->generateRandomString()
                )
            );
        }

        /**
         * Generates a random string.
         *
         * @return string A random alphanumeric string.
         */
        private function generateRandomString()
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $numCharacters = rand(1, 10);
            $string = '';

            for ($i = 0; $i < $numCharacters; $i++) {
                $string .= $characters[rand(0, strlen($characters) - 1)];
            }

            return $string;
        }


        /**
         * Tests if the registry returns null if a route cannot be found. Note that all parts of a URI must match.
         */
        public function test_getMatchingRoute_invalidRouteURI()
        {
            $route = new Route(HTTPMethod::GET, '/test/uri/signature', '', '');

            $this->registry->registerRoute($route);

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::GET,
                    '/invalid/uri/signature'
                )
            );

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::GET,
                    '/test/invalid/signature'
                )
            );

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::GET,
                    '/test/uri/invalid'
                )
            );
        }

        /**
         * Tests if the registry returns null if a route cannot be found. Note that all parts of a URI must match.
         */
        public function test_getMatchingRoute_invalidRouteMethod()
        {
            $signature = '/test/uri/signature';
            $route = new Route(HTTPMethod::GET, $signature, '', '');

            $this->registry->registerRoute($route);

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::POST,
                    $signature
                )
            );

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::PUT,
                    $signature
                )
            );

            $this->assertEquals(
                null,
                $this->registry->getMatchingRoute(
                    HTTPMethod::DELETE,
                    $signature
                )
            );
        }
    }
